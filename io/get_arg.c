/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_arg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 15:42:00 by lportay           #+#    #+#             */
/*   Updated: 2019/12/09 17:06:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "io.h"

/*
** the '*fmt' string is on a '%' character
*/

void	get_arg(t_buf *b, va_list *ap, const char **fmt)
{
	(*fmt)++;
	if (**fmt == '\0')
		return ;
	else if (**fmt == '%')
		buf_c(b, '%');
	else if (**fmt == 's')
		buf_s(b, va_arg(*ap, char *));
	else if (**fmt == 'S')
		buf_ws(b, va_arg(*ap, wchar_t *));
	else if (**fmt == 'u')
		buf_ul(b, va_arg(*ap, unsigned long));
	else if (**fmt == 'p')
		buf_x(b, va_arg(*ap, unsigned long), PREFIX, UPPER);
	else if (**fmt == 'i')
		buf_l(b, va_arg(*ap, long));
	(*fmt)++;
}
