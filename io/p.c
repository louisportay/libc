/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   p.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 15:35:08 by lportay           #+#    #+#             */
/*   Updated: 2019/12/09 17:07:13 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "io.h"

uint64_t	p(const char *fmt, ...)
{
	t_buf	buf;
	va_list	ap;

	buf_init(&buf, STDOUT_FILENO);
	va_start(ap, fmt);
	while (*fmt)
	{
		if (*fmt == '%')
			get_arg(&buf, &ap, &fmt);
		else
			buf_c(&buf, *fmt++);
	}
	va_end(ap);
	return (buf_flush(&buf));
}
