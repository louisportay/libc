IO=\
   gnl.c\
   puts.c\
   get_arg.c\
   p.c

IO_DIR=io

OBJ+=$(addprefix $(OBJDIR)/$(IO_DIR)/, $(IO:%.c=%.o))

SRCDIR+=$(IO_DIR)/

INCLUDE+=-I$(IO_DIR)/

HEADER=io

$(OBJDIR)/$(IO_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(IO_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(IO_DIR):
	mkdir -p $@
