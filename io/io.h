/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:48:42 by lportay           #+#    #+#             */
/*   Updated: 2019/12/09 17:07:28 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IO_H
# define IO_H

# include <stdlib.h>
# include <unistd.h>
# define IO_BUFF_SIZE 1024
# include "str.h"
# include "mem.h"
# include "buf.h"

int			gnl(const int fd, char **line);
int			dputs(const char *restrict s, const int fd);
void		get_arg(t_buf *b, va_list *ap, const char **fmt);
uint64_t	p(const char *fmt, ...);

#endif
