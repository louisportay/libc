/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puts.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 16:10:01 by lportay           #+#    #+#             */
/*   Updated: 2019/11/27 16:16:27 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "io.h"
#include "str.h"

int	dputs(const char *restrict s, const int fd)
{
	return(write(fd, s, ft_strlen(s)));
}
