/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_add_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 15:47:50 by lportay           #+#    #+#             */
/*   Updated: 2018/12/06 18:25:36 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	lst_addback(t_lst **l, t_lst *elem)
{
	t_lst *iter;

	if (!(*l))
		lst_addfront(l, elem);
	else
	{
		iter = *l;
		while (iter->next)
			iter = iter->next;
		iter->next = elem;
	}
}

int		lst_addback_data(t_lst **l, void *data)
{
	t_lst *new;
	t_lst *iter;

	if (!(*l))
		return (lst_addfront_data(l, data));
	if (!(new = lst_new(data)))
		return (-1);
	iter = *l;
	while (iter->next)
		iter = iter->next;
	iter->next = new;
	return (0);
}

int		lst_addback_dup(t_lst **l, void *data, void *(*dup)(void *))
{
	void	*cpy;
	t_lst	*new;
	t_lst	*iter;

	if (!(*l))
		return (lst_addfront_data(l, data));
	if (!(cpy = dup(data)))
		return (-1);
	if (!(new = lst_new(cpy)))
	{
		free(cpy);
		return (-1);
	}
	iter = *l;
	while (iter->next)
		iter = iter->next;
	iter->next = new;
	return (0);
}
