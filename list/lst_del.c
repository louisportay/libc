/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_del.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 16:48:05 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 13:38:37 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	*lst_dequeuefront(t_lst **l)
{
	t_lst	*elem;
	void	*ret;

	elem = *l;
	*l = elem->next;
	ret = elem->data;
	free(elem);
	return (ret);
}

void	lst_delfront(t_lst **l, void (*del)(void *))
{
	t_lst	*elem;

	elem = *l;
	*l = elem->next;
	del(elem->data);
	free(elem);
}

void	*lst_dequeueback(t_lst **l)
{
	t_lst	*elem;
	t_lst	*last;
	void	*ret;

	elem = *l;
	if (elem->next)
	{
		while (elem->next->next)
			elem = elem->next;
		last = elem->next;
		elem->next = NULL;
	}
	else
	{
		last = *l;
		*l = NULL;
	}
	ret = last->data;
	free(last);
	return (ret);
}

void	lst_delback(t_lst **l, void (*del)(void *))
{
	t_lst	*elem;
	t_lst	*last;

	elem = *l;
	if (elem->next)
	{
		while (elem->next->next)
			elem = elem->next;
		last = elem->next;
		elem->next = NULL;
	}
	else
	{
		last = *l;
		*l = NULL;
	}
	del(last->data);
	free(last);
}

void	lst_destroy(t_lst **l, void (*del)(void *))
{
	t_lst *elem;

	elem = *l;
	while (elem)
	{
		*l = elem->next;
		del(elem->data);
		free(elem);
		elem = *l;
	}
}
