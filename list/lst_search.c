/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_search.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 16:17:27 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:28:20 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_lst		*lst_search(t_lst *l, int (*slct)(void *))
{
	while (l)
	{
		if (slct(l->data) == 1)
			return (l);
		l = l->next;
	}
	return (NULL);
}

t_lst		*lst_select(t_lst *l, int (*cmp)(void *, void *))
{
	t_lst *elem;

	elem = l;
	while (l)
	{
		if (cmp(elem->data, l->data) > 0)
			elem = l;
		l = l->next;
	}
	return (elem);
}
