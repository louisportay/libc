/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_dup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 15:10:20 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 18:13:54 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_result	lst_dup(t_lst *l, void *(*dup)(void *), void (*del)(void *))
{
	t_result	r;
	t_lst		*iter;
	void		*cpy;

	r.data = NULL;
	if (!l)
		return (result_early(&r, EMPTY));
	if (lst_addfront_dup((t_lst **)&r.data, l->data, dup) == -1)
		return (result_early(&r, NOMEM));
	r.status = OK;
	l = l->next;
	iter = (t_lst *)r.data;
	while (l)
	{
		if (!(cpy = dup(l->data)) ||
				!(iter->next = lst_new(cpy)))
		{
			lst_destroy((t_lst **)&r.data, del);
			return (result_early(&r, NOMEM));
		}
		iter = iter->next;
		l = l->next;
	}
	return (r);
}
