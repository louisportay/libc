/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_filter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 17:13:22 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:27:32 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static t_result	clear(t_result *r, void (*del)(void *))
{
	lst_destroy((t_lst **)&r->data, del);
	r->status = NOMEM;
	return (*r);
}

static void		fast_forward(t_lst **l, int (*slct)(void *))
{
	while (*l && slct((*l)->data) == 0)
		*l = (*l)->next;
}

/*
** To TEST
*/

t_result		lst_filter(t_lst *l, int (*slct)(void *), void *(*dup)(void *),
						void (*del)(void *))
{
	t_result	r;
	t_lst		*iter;
	void		*cpy;

	r.data = NULL;
	fast_forward(&l, slct);
	if (!l)
		return (result_early(&r, EMPTY));
	else if (lst_addfront_dup((t_lst **)&r.data, l->data, dup) == -1)
		return (result_early(&r, NOMEM));
	r.status = OK;
	l = l->next;
	iter = (t_lst *)r.data;
	while (l)
	{
		if (slct(l->data) == 1)
		{
			if (!(cpy = dup(l->data)) ||
					!(iter->next = lst_new(cpy)))
				return (clear(&r, del));
			iter = iter->next;
		}
		l = l->next;
	}
	return (r);
}
