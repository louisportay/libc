OTHER=\
	  max.c

OTHER_DIR=other

OBJ+=$(addprefix $(OBJDIR)/$(OTHER_DIR)/, $(OTHER:%.c=%.o))

INCLUDE+=-I$(OTHER_DIR)/

SRCDIR+=$(OTHER_DIR)/

HEADER=other

$(OBJDIR)/$(OTHER_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(OTHER_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(OTHER_DIR):
	mkdir -p $@
