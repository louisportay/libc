/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_destroy.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 11:24:46 by lportay           #+#    #+#             */
/*   Updated: 2018/12/09 19:38:05 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

void	stack_destroy(t_stk **stk, void (*del)(void *))
{
	t_stk *elem;

	elem = *stk;
	while (elem)
	{
		*stk = elem->down;
		del(elem->data);
		free(elem);
		elem = *stk;
	}
}
