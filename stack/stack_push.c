/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 19:30:21 by lportay           #+#    #+#             */
/*   Updated: 2018/12/09 19:37:28 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

void	stack_push(t_stk **stk, t_stk *member)
{
	member->down = *stk;
	*stk = member;
}

int		stack_push_data(t_stk **stk, void *data)
{
	t_stk *new;

	if (!(new = stack_new(data)))
		return (-1);
	new->down = *stk;
	*stk = new;
	return (0);
}

int		stack_push_dup(t_stk **stk, void *data, void *(*dup)(void *))
{
	void	*cpy;
	t_stk	*new;

	if (!(cpy = dup(data)))
		return (-1);
	if (!(new = stack_new(cpy)))
	{
		free(cpy);
		return (-1);
	}
	new->down = *stk;
	*stk = new;
	return (0);
}
