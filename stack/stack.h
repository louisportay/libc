/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 17:44:43 by lportay           #+#    #+#             */
/*   Updated: 2018/12/09 20:14:21 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H

# include <stdlib.h>
# include "result.h"

typedef struct		s_stack
{
	struct s_stack	*down;
	void			*data;
}					t_stk;

t_stk				*stack_new(void *data);
void				stack_push(t_stk **stk, t_stk *member);
int					stack_push_data(t_stk **stk, void *data);
int					stack_push_dup(t_stk **stk, void *data,
									void *(*dup)(void *));
void				*stack_pop(t_stk **stk);
void				stack_del(t_stk **stk, void (*del)(void *));
void				stack_destroy(t_stk **stk, void (*del)(void *));
void				stack_map(t_stk *stk, void (*fn)(void *));
t_result			stack_dup(t_stk *stk, void *(*dup)(void *),
								void (*del)(void *));
unsigned			stack_depth(t_stk *stk);

/*
** NEW
**   DESCRIPTION
**     create a new member containing 'data' without duplicating it
**   RETURN
**     NULL => Error allocating memory
**
** PUSH
**   DESCRIPTION
**     stack a new member on top of the existing stack
**   ARGUMENTS
**     'stk' MUST BE NOT NULL
**
** PUSH_DATA
**   DESCRIPTION
**     syntactic sugar for NEW + PUSH
**   ARGUMENTS
**     'stk' MUST BE NOT NULL
**   RETURN
**     -1 => Error allocating memory
**
** PUSH_DUP
**   DESCRIPTION
**     Make a duplicate of 'data' with 'dup' function and then add it
**		on top of 'stk'
**   ARGUMENTS
**     'stk' MUST BE NOT NULL
**     'dup' NUST BE NOT NULL
**   RETURN
**     -1 => Error allocating memory
**
** POP
**   DESCRIPTION
**     Return the data of the last member in the stack and free its container
**   ARGUMENTS
**     'st' MUST BE NOT NULL
**
** DESTROY
**   DESCRIPTION
**     Destroy the whole stack with the given 'del' function to free up the data
**   ARGUMENTS
**     'stk' MUST BE NOT NULL
**     'del' MUST BE NOT NULL
**   RETURN
**
** MAP /!\ Mutate in place
**
** DUP
**   DESCRIPTION
**     duplicate the whole 'stk' with 'dup' function, if it fails at one point,
**		destroy the already created stack with 'del'
**   ARGUMENTS
**     ALL ARGUMENTS MUST BE NOT NULL
**   RETURN
**     NULL => Error allocating memory
**
** DEPTH
**   DESCRIPTION
**     returns the number of members in the stack
**   RETURN
**     0 => Empty stack (NULL)
**
** EMPTY
**   DESCRIPTION
**     true if there is something, false otherwise
*/

#endif
