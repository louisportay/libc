/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 19:23:07 by lportay           #+#    #+#             */
/*   Updated: 2018/12/11 12:13:55 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

unsigned	stack_depth(t_stk *stk)
{
	unsigned depth;

	depth = 0;
	while (stk)
	{
		depth++;
		stk = stk->down;
	}
	return (depth);
}

void		stack_rev(t_stk **stk)
{
	t_stk *prev;
	t_stk *current;
	t_stk *next;

	prev = NULL;
	current = *stk;
	while (current)
	{
		next = current->down;
		current->down = prev;
		prev = current;
		current = next;
	}
	*stk = prev;
}
