/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 18:02:37 by lportay           #+#    #+#             */
/*   Updated: 2019/12/09 17:13:01 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "err.h"

static void	get_arg_UNSTABLE(t_buf *b, va_list *ap, const char **fmt)
{
	(*fmt)++;
	if (**fmt == '\0')
		return ;
	else if (**fmt == '%')
		buf_c(b, '%');
	else if (**fmt == 's')
		buf_s(b, va_arg(*ap, char *));
	else if (**fmt == 'S')
		buf_ws(b, va_arg(*ap, wchar_t *));
	else if (**fmt == 'u')
		buf_ul(b, va_arg(*ap, unsigned long));
	else if (**fmt == 'p')
		buf_x(b, va_arg(*ap, unsigned long), PREFIX, UPPER);
	else if (**fmt == 'i')
		buf_l(b, va_arg(*ap, long));
	(*fmt)++;
}
static void dump_err(const char *fmt, va_list *ap)
{
	t_buf	buf;

	buf_init(&buf, STDERR_FILENO);
	while (*fmt)
	{
		if (*fmt == '%')
			get_arg_UNSTABLE(&buf, ap, &fmt);
		else
			buf_c(&buf, *fmt++);
	}
	buf_flush(&buf);
}

int32_t		fmt_err(const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	dump_err(fmt, &ap);
	va_end(ap);
	return (-1);
}

void	fatal(int ret, int err, const char *fmt, ...)
{
	va_list ap;

	if (ret == err)
	{
		va_start(ap, fmt);
		dump_err(fmt, &ap);
		va_end(ap);
		exit(-1);
	}
}

void	fatalp(void *ret, void *err, const char *fmt, ...)
{
	va_list ap;

	if (ret == err)
	{
		va_start(ap, fmt);
		dump_err(fmt, &ap);
		va_end(ap);
		exit(-1);
	}
}
