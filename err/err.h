/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:48:42 by lportay           #+#    #+#             */
/*   Updated: 2019/12/09 15:41:30 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERR_H
# define ERR_H

/*
** Accepted flags includes
**
** %s : str
** %S : wide str
** %u : uint64_t
** %i : int64_t
*/

# include <stdarg.h>
# include "buf.h"
# include "io.h"

int32_t	fmt_err(const char *fmt, ...);
void    fatal(int ret, int err, const char *fmt, ...);
void    fatalp(void *ret, void *err, const char *fmt, ...);

#endif
