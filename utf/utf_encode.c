/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utf_encode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:19:23 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 13:31:35 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utf.h"

void	two_bytes_char(char **s, const wchar_t **w_str)
{
	**s = ((**w_str >> 6) & 0b11111) | 0b11000000;
	(*s)++;
	**s = (**w_str & 0b111111) | 0b10000000;
}

void	three_bytes_char(char **s, const wchar_t **w_str)
{
	**s = ((**w_str >> 12) & 0b1111) | 0b11100000;
	(*s)++;
	**s = ((**w_str >> 6) & 0b111111) | 0b10000000;
	(*s)++;
	**s = (**w_str & 0b111111) | 0b10000000;
}

void	four_bytes_char(char **s, const wchar_t **w_str)
{
	**s = ((**w_str >> 18) & 0b111) | 0b11110000;
	(*s)++;
	**s = ((**w_str >> 12) & 0b111111) | 0b10000000;
	(*s)++;
	**s = ((**w_str >> 6) & 0b111111) | 0b10000000;
	(*s)++;
	**s = (**w_str & 0b111111) | 0b10000000;
}

char	*utf_encode(const wchar_t *w_str, size_t len)
{
	char *s;
	char *iter;

	if (!(s = strnew(wcsnbytes(w_str, len))))
		return (NULL);
	iter = s;
	while (len--)
	{
		if (isunicode(*w_str))
		{
			if (*w_str <= 0x7f)
				*iter = *w_str;
			else if (*w_str <= 0x7FF)
				two_bytes_char(&iter, &w_str);
			else if (*w_str <= 0xFFFF)
				three_bytes_char(&iter, &w_str);
			else if (*w_str <= 0x1FFFFF)
				four_bytes_char(&iter, &w_str);
			iter++;
		}
		w_str++;
	}
	return (s);
}
