/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utf_decode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:37:33 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 13:45:29 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utf.h"

static void	two_bytes_char(const char **str, wchar_t **s)
{
	**s |= ((**str & 0b00011111) << 6);
	(*str)++;
	**s |= (**str & 0b00111111);
}

static void	three_bytes_char(const char **str, wchar_t **s)
{
	**s |= ((**str & 0b00001111) << 12);
	(*str)++;
	**s |= ((**str & 0b00111111) << 6);
	(*str)++;
	**s |= (**str & 0b00111111);
}

static void	four_bytes_char(const char **str, wchar_t **s)
{
	**s |= ((**str & 0b00000111) << 18);
	(*str)++;
	**s |= ((**str & 0b00111111) << 12);
	(*str)++;
	**s |= ((**str & 0b00111111) << 6);
	(*str)++;
	**s |= (**str & 0b00111111);
}

wchar_t		*utf_decode(const char *str, size_t len)
{
	wchar_t	*s;
	wchar_t *iter;

	if (!(s = wcsnew(len)))
		return (NULL);
	iter = s;
	while (len--)
	{
		if (*str >= 0)
			*iter = *str;
		else
		{
			if ((*str & 0b11100000) == 0b11000000)
				two_bytes_char(&str, &iter);
			else if ((*str & 0b11110000) == 0b11100000)
				three_bytes_char(&str, &iter);
			else if ((*str & 0b11111000) == 0b11110000)
				four_bytes_char(&str, &iter);
		}
		iter++;
		str++;
	}
	return (s);
}
