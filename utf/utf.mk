UTF=\
	utf_decode.c\
	utf_encode.c\

UTF_DIR=utf

OBJ+=$(addprefix $(OBJDIR)/$(UTF_DIR)/, $(UTF:%.c=%.o))

SRCDIR+=$(UTF_DIR)/

INCLUDE+=-I$(UTF_DIR)/

HEADER=utf

$(OBJDIR)/$(UTF_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(UTF_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(UTF_DIR):
	mkdir -p $@
