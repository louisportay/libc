STRING=\
s_append.c\
s_cmp.c\
s_del.c\
s_dup.c\
s_extract.c\
s_grow.c\
s_len.c\
s_new.c\
s_print.c\
s_wappend.c\
s_wextract.c\
s_wnew.c\

STRING_DIR=string

OBJ+=$(addprefix $(OBJDIR)/$(STRING_DIR)/, $(STRING:%.c=%.o))

SRCDIR+=$(STRING_DIR)/

INCLUDE+=-I$(STRING_DIR)/

HEADER=string

$(OBJDIR)/$(STRING_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(STRING_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(STRING_DIR):
	mkdir -p $@
