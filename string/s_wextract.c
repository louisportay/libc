/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_wextract.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:11:27 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:12:31 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

wchar_t	*s_wextract(t_string *s)
{
	wchar_t *string;

	string = s->s;
	free(s);
	return (string);
}
