/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_grow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 12:51:28 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 15:00:35 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

int	s_grow(t_string *s)
{
	wchar_t *lengthier_s;

	if (!(lengthier_s = malloc(sizeof(wchar_t) * (s->size * 2))))
		return (-1);
	ft_wcsncpy(lengthier_s, s->s, s->size);
	free(s->s);
	s->s = lengthier_s;
	s->size *= 2;
	return (0);
}
