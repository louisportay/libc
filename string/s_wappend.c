/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_wappend.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:06:20 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:10:11 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

int	s_wappend(const wchar_t *w_str, t_string *s)
{
	size_t	w_len;

	w_len = ft_wcslen(w_str);
	while (s->used + w_len > s->size)
		if (s_grow(s) == -1)
			return (-1);
	ft_wcscat(s->s, w_str);
	s->used += w_len;
	return (0);
}
