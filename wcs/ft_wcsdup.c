/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 19:30:00 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 15:08:29 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wcs.h"

wchar_t	*ft_wcsdup(const wchar_t *s)
{
	wchar_t *dup;
	wchar_t *iter;

	if (!(dup = malloc(sizeof(wchar_t) * (ft_wcslen(s) + 1))))
		return (NULL);
	iter = dup;
	while (*s)
	{
		*iter = *s;
		s++;
		iter++;
	}
	*iter = '\0';
	return (dup);
}

wchar_t	*ft_wcsndup(const wchar_t *s, size_t n)
{
	wchar_t	*dup;
	wchar_t	*iter;
	size_t	len;

	len = ft_wcslen(s);
	if (len < n)
		n = len;
	if (!(dup = (wchar_t *)malloc(sizeof(wchar_t) * (n + 1))))
		return (NULL);
	iter = dup;
	while (*s && n)
	{
		*iter = *s;
		s++;
		iter++;
		n--;
	}
	*iter = '\0';
	return (dup);
}
