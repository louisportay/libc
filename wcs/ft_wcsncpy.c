/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcsncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 11:36:50 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 15:01:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wcs.h"

wchar_t	*ft_wcsncpy(wchar_t *dest, const wchar_t *src, size_t n)
{
	wchar_t *iter;

	iter = dest;
	while (n > 0 && *src != L'\0')
	{
		*iter++ = *src++;
		n--;
	}
	while (n > 0)
	{
		*iter++ = L'\0';
		n--;
	}
	return (dest);
}
