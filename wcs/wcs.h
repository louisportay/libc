/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wcs.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 13:15:02 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:55:19 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WCS_H
# define WCS_H

# include <stdlib.h>
# include <wchar.h>
# include "is.h"

wchar_t	*ft_wcscpy(wchar_t *dest, const wchar_t *src);
wchar_t *ft_wcsncpy(wchar_t *dest, const wchar_t *src, size_t n);
wchar_t	*ft_wcsdup(const wchar_t *s);
wchar_t	*ft_wcsndup(const wchar_t *s, size_t n);
int		ft_wcscmp(const wchar_t *s1, const wchar_t *s2);
int		ft_wcsncmp(const wchar_t *s1, const wchar_t *s2, size_t n);
wchar_t	*ft_wcscat(wchar_t *dest, const wchar_t *src);
wchar_t	*ft_wcsncat(wchar_t *dest, const wchar_t *src, size_t n);
size_t	ft_wcslen(const wchar_t *s);
wchar_t	*wcsnew(size_t size);
size_t	wcsbytes(const wchar_t *s);
size_t	wcsnbytes(const wchar_t *s, size_t len);

#endif
