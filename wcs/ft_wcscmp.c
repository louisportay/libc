/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcscmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:38:11 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:58:32 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wcs.h"

int	ft_wcscmp(const wchar_t *s1, const wchar_t *s2)
{
	while (*s1 == *s2)
	{
		if (*s1++ == '\0')
			return (0);
		s2++;
	}
	return (*s1 - *s2);
}

int	ft_wcsncmp(const wchar_t *s1, const wchar_t *s2, size_t n)
{
	if (n == 0)
		return (0);
	while (*s1 == *s2)
	{
		if (*s1++ == '\0' || --n <= 0)
			return (0);
		s2++;
	}
	return (*s1 - *s2);
}
