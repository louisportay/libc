/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wcsnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 19:26:21 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 15:44:10 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Renvoie une chaîne propre de "size" caracteres + 1 ('\0' final)
*/

#include "wcs.h"

wchar_t	*wcsnew(size_t size)
{
	wchar_t	*s;
	wchar_t	*iter;

	if (!(s = (wchar_t *)malloc(sizeof(wchar_t) * (size + 1))))
		return (NULL);
	iter = s;
	while (size--)
		*iter++ = '\0';
	*iter = '\0';
	return (s);
}
