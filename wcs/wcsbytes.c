/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wcsbytes.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:21:49 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 19:51:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wcs.h"

/*
** 127 -> 7 bits
** 2047 -> 11 bits
** 65535 -> 16 bits
** 2097151 -> 21 bits
*/

size_t	wcsbytes(const wchar_t *s)
{
	size_t bytes;

	bytes = 0;
	while (*s)
	{
		if (isunicode(*s))
		{
			if (*s >= 0 && *s <= 127)
				bytes += 1;
			else if (*s >= 128 && *s <= 2047)
				bytes += 2;
			else if (*s >= 2048 && *s <= 65535)
				bytes += 3;
			else if (*s >= 65536 && *s <= 2097151)
				bytes += 4;
		}
		s++;
	}
	return (bytes);
}

size_t	wcsnbytes(const wchar_t *s, size_t len)
{
	size_t bytes;

	bytes = 0;
	while (len--)
	{
		if (isunicode(*s))
		{
			if (*s >= 0 && *s <= 127)
				bytes += 1;
			else if (*s >= 128 && *s <= 2047)
				bytes += 2;
			else if (*s >= 2048 && *s <= 65535)
				bytes += 3;
			else if (*s >= 65536 && *s <= 2097151)
				bytes += 4;
		}
		s++;
	}
	return (bytes);
}
