/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_append.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 11:31:18 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 12:58:50 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

int		vec_append(t_vector *vec, void *data)
{
	int u;

	if (vec->used == vec->size)
		if (vec_grow(vec) == -1)
			return (-1);
	u = 0;
	while (vec->map[u])
		u++;
	vec->map[u] = data;
	vec->used++;
	return (u);
}

int		vec_append_dup(t_vector *vec, void *data, void *(*dup)(void *))
{
	int u;

	if (!(data = dup(data)))
		return (-1);
	if (vec->used == vec->size)
		if (vec_grow(vec) == -1)
			return (-1);
	u = 0;
	while (vec->map[u])
		u++;
	vec->map[u] = data;
	vec->used++;
	return (u);
}
