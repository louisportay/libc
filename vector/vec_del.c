/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_del.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 12:06:58 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 11:26:20 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void	*vec_extract(t_vector *vec, unsigned i)
{
	void *data;

	data = vec->map[i];
	vec->map[i] = NULL;
	return (data);
}

void	vec_del(t_vector *vec, unsigned i, void (*del)(void *))
{
	del(vec->map[i]);
	vec->map[i] = NULL;
}

void	vec_destroy(t_vector *vec, void (*del)(void *))
{
	while (vec->size > 0)
	{
		if (vec->map[--vec->size])
			del(vec->map[vec->size]);
	}
	free(vec->map);
	vec->map = NULL;
}
