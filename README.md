This constitutes the main documentation for my own library.

Every module is documented inside his own header file.

Module list:
#### Abstract Data Types (ADT)
* Stack (LIFO)
* Linked list (FIFO)
* Hashmap (lookup table, dictionnary, name it as you like...)
* Dynamic Arrays (upgraded 'array' type)

* Doulbly-linked list
* Binary trees

#### Concrete Data Types
* Dynamic Strings ('str' slices upgraded)
* String slices


#### Other libraries
* Assert
* Colors
* Result

Common operations on ADT includes:
* Create new member (new)
* Add new member(s) to the existing structure (add)
* delete member(s) (del)
* destroy the existing structure (destroy)
* Total number of members (count)
* Search for an element (search)
* Sort members (sort)
* Apply some function to all the members (map)
* Select only members matching a condition (filter)

Common operations on CDT
* Print the structure		(`void (*print)(void *)`)
* Duplicate member(s)		(`void *(*dup)(void *)`)
* Delete member(s)			(`void (*del)(void *)`)
* Compare two member(s)		(`int (*cmp)(void *, void *)`)
* Length of a member (len)	(`size_t (*len)(void *)`)
* Test equality (eq)		(`int (*eq)(void *, void *)`)
* Selector (slct)			(`int (*slct)(void *)`)

## TODO

Constify the whole library as much as possible  
