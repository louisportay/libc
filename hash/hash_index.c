/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_index.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 16:10:52 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 21:16:47 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"

/*
** Return the index associated with a given key.
**
** Casual stuff
*/

unsigned	hash_index(void *key)
{
	return ((unsigned long)key % HASHSIZE);
}
