/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_new.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 19:18:35 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 18:28:34 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"

/*
** Allocate a bucket with a 'key' and some 'data' inside of it
*/

t_result	hash_new(void *key, void *data)
{
	t_result r;

	if (!key)
		return (result_early(&r, ERR));
	if (!(r.data = (t_hash *)malloc(sizeof(t_hash))))
		return (result_early(&r, NOMEM));
	r.status = OK;
	((t_hash *)r.data)->key = key;
	((t_hash *)r.data)->data = data;
	return (r);
}
