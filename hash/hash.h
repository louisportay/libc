/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 16:01:58 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 16:19:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASH_H
# define HASH_H

# define HASHSIZE 32

# include <stdlib.h>
# include "result.h"
# include "kvp.h"

typedef struct		s_hash
{
	void			*key;
	void			*data;
	struct s_hash	*next;
}					t_hash;

typedef struct		s_hash_utils
{
	int				(*eq)(void *, void *);
	void			*(*dup_key)(void *);
	void			*(*dup_data)(void *);
	void			(*del_key)(void *);
	void			(*del_data)(void *);
}					t_hash_utils;

t_result			hash_new(void *key, void *data);
void				hash_initalize(t_hash **table);
unsigned			hash_index(void *key);
t_hash				*hash_lookup(t_hash **table, void *key, t_hash_utils *u);
void				hash_insert(t_hash **table, t_hash *bucket, int crush,
								t_hash_utils *u);
int					hash_insert_data(t_hash **table, t_kvp p, int crush,
										t_hash_utils *u);
int					hash_insert_dup(t_hash **table, t_kvp p, int crush,
									t_hash_utils *u);
t_kvp				hash_extract(t_hash **table, void *key, t_hash_utils *u);
void				hash_del(t_hash **table, void *key, t_hash_utils *u);
void				hash_clear(t_hash **table, t_hash_utils *u);

#endif
