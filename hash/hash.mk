HASH=\
	hash_new.c\
	hash_index.c\
	hash_insert.c\
	hash_lookup.c\
	hash_del.c\
	hash_utils.c\


HASH_DIR=hash

OBJ+=$(addprefix $(OBJDIR)/$(HASH_DIR)/, $(HASH:%.c=%.o))

SRCDIR+=$(HASH_DIR)/

INCLUDE+=-I$(HASH_DIR)/

HEADER=hash

$(OBJDIR)/$(HASH_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(HASH_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(HASH_DIR):
	mkdir -p $@
